package application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;

public class GUIcontroller {
	
	BuildingHandler buildingHandler;

	@FXML
	private ListView<String> buildings;

	@FXML
	private TabPane levels;

	@FXML
	ComboBox<String> type;

	@FXML
	ComboBox<String> tileReq;

	@FXML
	Spinner<Integer> sizeX;

	@FXML
	Spinner<Integer> sizeY;

	@FXML
	private Tab pane1;

	@FXML
	private Tab pane2;

	@FXML
	private Tab pane3;

	private FXMLLoader loader;

	private ArrayList<LevelController> controllers;

	public void initialize() {
		buildingHandler = new BuildingHandler();
		controllers = new ArrayList<LevelController>();

		sizeX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 20, 0, 1));
		sizeY.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 20, 0, 1));
		
		try {
			loader = new FXMLLoader(getClass().getResource("/application/Level.fxml"));
			Pane pane = loader.load();
			pane1.setContent(pane);
			controllers.add(loader.getController());
			controllers.get(0).setUp(1);
			
			loader = new FXMLLoader(getClass().getResource("/application/Level.fxml"));
			pane2.setContent(loader.load());
			controllers.add(loader.getController());
			controllers.get(1).setUp(2);
			
			loader = new FXMLLoader(getClass().getResource("/application/Level.fxml"));
			pane3.setContent(loader.load());
			controllers.add(loader.getController());
			controllers.get(2).setUp(3);
			
			for (String r : controllers.get(0).resources)
			{
				tileReq.getItems().add(r);				
			}



			
		} catch (IOException e) {
			e.printStackTrace();
		}
		readFromFile();
		setBuildings();
	}
	
	public void readFromFile()
	{
		BufferedReader in = null;
		ArrayList<String> types = new ArrayList<String>();
		try {
			in = new BufferedReader(new FileReader("TypeNames.data"));
			String temp = "wda";
			while ((temp = in.readLine()) != null) {

				types.add(temp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (String r : types)
		{
			type.getItems().add(r);
		}
	}
	
	private void setBuildings()
	{

		for(int i = 0; i < buildingHandler.buildings.size(); ++i) 
		{

			buildings.getItems().add(buildingHandler.getBuildingName(i,0));

		}
	}
	
	@FXML
	private void editBuilding()
	{
		emptyBuilding();
		Building temp;
		for(int i = 0; i < buildingHandler.buildings.size(); ++i)
		{
			if (buildingHandler.buildings.get(i).getBuildingName(0).equals(buildings.getSelectionModel().getSelectedItem()))
			{
				temp = buildingHandler.buildings.get(i);
				for(int j = 0; j < 3; ++j)
				{
					controllers.get(j).editLevel(temp.levels.get(j));			
				}
				type.getSelectionModel().select(temp.type);
				sizeX.getValueFactory().setValue(temp.sizeX);
				sizeY.getValueFactory().setValue(temp.sizeY);
				tileReq.getSelectionModel().select(temp.tileReq);
			}
		}
		
	}
	@FXML
	private void saveBuilding()
	{
		

		ArrayList<BuildingLevel> tempLevels = new ArrayList<BuildingLevel>();
		for (int i = 0; i < buildingHandler.buildings.size(); ++i)
		{
			if (buildingHandler.buildings.get(i).levels.get(0).name.equals(controllers.get(0).name.getText()))
			{
				buildingHandler.buildings.remove(i);
				buildings.getItems().remove(i);
			}		
		}
		for (int i = 0; i < 3; ++i)
		{

			tempLevels.add(controllers.get(i).saveLevel());
		}
		Building temp = new Building(sizeX.getValue(), sizeY.getValue(),
									tileReq.getSelectionModel().getSelectedItem(),
									type.getSelectionModel().getSelectedIndex(),
									tempLevels);
		buildingHandler.buildings.add(temp);
		buildings.getItems().add(temp.levels.get(0).name);
	}
	
	@FXML
	private void emptyBuilding()
	{
		type.getSelectionModel().clearSelection();
		sizeX.getValueFactory().setValue(0);
		sizeY.getValueFactory().setValue(0);
		tileReq.getSelectionModel().clearSelection();
		for(int i = 0; i < 3; ++i)
		{
			controllers.get(i).clearLevel();
		}
		
	}
	
	@FXML
	private void printBuildings()
	{
		buildingHandler.printBuildings();
	}
	
	@FXML
	private void deleteBuilding()
	{
		buildingHandler.buildings.remove(buildings.getSelectionModel().getSelectedIndex());
		buildings.getItems().remove(buildings.getSelectionModel().getSelectedIndex());
	}
}
