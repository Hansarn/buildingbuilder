package application;
	
import java.util.ArrayList;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;


public class Main extends Application {
	public ArrayList<Building> buildings;
	 private FXMLLoader loader;
	@Override
	public void start(Stage primaryStage) {
		try {
		      loader = new FXMLLoader(getClass().getResource("/application/GUI.fxml"));
		      Pane pane = (Pane)loader.load();
		      Scene scene = new Scene(pane);
		      @SuppressWarnings("unused")
			GUIcontroller c = loader.getController();
		      primaryStage.setScene(scene);
		      primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
