package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class BuildingHandler {
	public ArrayList<Building> buildings;
	public BuildingHandler()
	{
		buildings = new ArrayList<Building>();
		readFromFile();
	}
	void readFromFile()
	{

		    BufferedReader in = null;
		    
		    StringBuffer sb = new StringBuffer();
		    try {
		      in = new BufferedReader(new FileReader("../Buildings.data"));
		      String temp = "wda";
		      while ((temp = in.readLine()) != null) {	
		        sb.setLength(0);
		        int sizeX = Integer.parseInt(temp);
		        temp = in.readLine();
		        int sizeY = Integer.parseInt(temp);
		        temp = in.readLine();
		        String tileReq = temp;
		        temp = in.readLine();
		        int type = Integer.parseInt(temp);
		        temp = in.readLine();
		        ArrayList<BuildingLevel> levels = new ArrayList<BuildingLevel>();
		        for (int i = 0; i < 3; ++i)
		        {
		        	sb = new StringBuffer();
		        	String name = temp;
		        	temp = in.readLine();
			        while (!temp.equals("}")) {
				          sb.append(temp + "\n");
				          temp = in.readLine();
				        }
			        temp = in.readLine();
			        sb.setLength(sb.length() - 2);
			        String description = sb.toString();

			        int maxPawns = Integer.parseInt(temp);	
			        temp = in.readLine();
			        ArrayList<String> resOutput = new ArrayList<String>();
			        ArrayList<String> resOutputAmount = new ArrayList<String>();
			        while (!temp.equals("}"))
			        {
			        	resOutput.add(temp);
			        	temp = in.readLine();
			        	resOutputAmount.add(temp);
			        	temp = in.readLine();
			        }
			        temp = in.readLine();
			        ArrayList<String> tradeOutput = new ArrayList<String>();
			        ArrayList<String> tradeOutputAmount = new ArrayList<String>();
			        while (!temp.equals("}"))
			        {
			        	tradeOutput.add(temp);
			        	temp = in.readLine();
			        	tradeOutputAmount.add(temp);
			        	temp = in.readLine();
			        }
			        temp = in.readLine();

			        ArrayList<String> resPrice = new ArrayList<String>();
			        ArrayList<String> resPriceAmount = new ArrayList<String>();
			        while (!temp.equals("}"))
			        {
			        	resPrice.add(temp);
			        	temp = in.readLine();
			        	resPriceAmount.add(temp);
			        	temp = in.readLine();
			        }
			        temp = in.readLine();
			        ArrayList<String> tradePrice = new ArrayList<String>();
			        ArrayList<String> tradePriceAmount = new ArrayList<String>();
			        while (!temp.equals("}"))
			        {
			        	tradePrice.add(temp);
			        	temp = in.readLine();
			        	tradePriceAmount.add(temp);
			        	temp = in.readLine();
			        }
			        temp = in.readLine();

			        ArrayList<String> resCost = new ArrayList<String>();
			        ArrayList<String> resCostAmount = new ArrayList<String>();
			        while (!temp.equals("}"))
			        {
			        	resCost.add(temp);
			        	temp = in.readLine();
			        	resCostAmount.add(temp);
			        	temp = in.readLine();
			        }
			        temp = in.readLine();
			        ArrayList<String> tradeCost = new ArrayList<String>();
			        ArrayList<String> tradeCostAmount = new ArrayList<String>();
			        while (!temp.equals("}"))
			        {
			        	tradeCost.add(temp);
			        	temp = in.readLine();
			        	tradeCostAmount.add(temp);
			        	temp = in.readLine();
			        }
			        temp = in.readLine();
			        String model = temp;
			        ArrayList<String> research = new ArrayList<String>();
			        temp = in.readLine();
			        while (!temp.equals("}"))
			        {

			        	research.add(temp);
			        	
			        	temp = in.readLine();
			        }
			        temp = in.readLine();
			        int time = Integer.parseInt(temp);
			        temp = in.readLine();
			        BuildingLevel level = new BuildingLevel(name,description,
			        						maxPawns,resPrice,resPriceAmount,
			        						tradePrice,tradePriceAmount,
			        						resOutput,resOutputAmount,
			        						tradeOutput,tradeOutputAmount,
			        						resCost,resCostAmount,
			        						tradeCost,tradeCostAmount,
			        						model,research,time);
			        levels.add(level);

		        }
		        

		        


		        Building r = new Building(sizeX, sizeY, tileReq, type, levels);
		        buildings.add(r);
		      }
		    }
		    catch (IOException e)
		    {
		      e.printStackTrace();
		      
		      try
		      {
		        if (in != null) {
		          in.close();
		        }
		      }
		      catch (IOException e1)
		      {
		        e1.printStackTrace();
		      }
		    }
		    finally
		    {
		      try
		      {
		        if (in != null) {
		          in.close();
		        }
		      }
		      catch (IOException e)
		      {
		        e.printStackTrace();
		      }
		    }
	}
	public String getBuildingName(int i, int j)
	{

		return buildings.get(i).getBuildingName(j);

	}
	
	public void printBuildings()
	{
		try {
		BufferedWriter writer = new BufferedWriter(new FileWriter("../Buildings.data"));
		for (int i = 0; i < buildings.size(); ++i)
		{
			buildings.get(i).print(writer);			
		}

			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
