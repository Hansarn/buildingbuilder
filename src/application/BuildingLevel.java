package application;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class BuildingLevel {
	public String name;
	public String description;
	public int maxPawns;
	public int buildTime;
	
	public ArrayList<String> resPrice;
	public ArrayList<String> resOutput;
	public ArrayList<String> resCost;
	public ArrayList<String> tradePrice;
	public ArrayList<String> tradeOutput;
	public ArrayList<String> tradeCost;
	
	public ArrayList<String> resPriceAmount;
	public ArrayList<String> resOutputAmount;
	public ArrayList<String> resCostAmount;
	public ArrayList<String> tradePriceAmount;
	public ArrayList<String> tradeOutputAmount;
	public ArrayList<String> tradeCostAmount;

	public ArrayList<String> researches;
	public String model;
	//Mother of all constructors
	public BuildingLevel(String name, String description, int maxPawns,
						 ArrayList<String> resPrice, ArrayList<String> resPriceAmount,
						 ArrayList<String> tradePrice, ArrayList<String> tradePriceAmount,
						 ArrayList<String> resOutput, ArrayList<String> resOutputAmount,
						 ArrayList<String> tradeOutput, ArrayList<String> tradeOutputAmount,
						 ArrayList<String> resCost, ArrayList<String> resCostAmount,						 
						 ArrayList<String> tradeCost, ArrayList<String> tradeCostAmount,
						 String model, ArrayList<String> researches, int buildTime)						
	{
		this.setName(name);
		
		this.description = description;
		
		this.maxPawns = maxPawns;
		
		this.buildTime = buildTime;
		
		this.resPrice = resPrice;
		this.resOutput = resOutput;
		this.resCost = resCost;
		
		this.tradePrice = tradePrice;
		this.tradeOutput = tradeOutput;
		this.tradeCost = tradeCost;
		
		this.resPriceAmount = resPriceAmount;
		this.resOutputAmount = resOutputAmount;
		this.resCostAmount = resCostAmount;
		
		this.tradePriceAmount = tradePriceAmount;
		this.tradeOutputAmount = tradeOutputAmount;
		this.tradeCostAmount = tradeCostAmount;
		this.researches = researches;

		this.model = model;

	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public void print(BufferedWriter w)
	{
		StringBuffer write = new StringBuffer();
		
		write.append(name + "\n" + description + "\n}\n" + Integer.toString(maxPawns) + "\n");
		
		for(int i = 0; i < resOutput.size(); ++i)
		{
			write.append(resOutput.get(i) + "\n" + resOutputAmount.get(i) + "\n");
		}
		write.append("}\n");
		for(int i = 0; i < tradeOutput.size(); ++i)
		{
			write.append(tradeOutput.get(i) + "\n" + tradeOutputAmount.get(i) + "\n");
		}
		write.append("}\n");
		for(int i = 0; i < resPrice.size(); ++i)
		{
			write.append(resPrice.get(i) + "\n" + resPriceAmount.get(i) + "\n");
		}
		write.append("}\n");
		for(int i = 0; i < tradePrice.size(); ++i)
		{
			write.append(tradePrice.get(i) + "\n" + tradePriceAmount.get(i) + "\n");
		}
		write.append("}\n");
		for(int i = 0; i < resCost.size(); ++i)
		{
			write.append(resCost.get(i) + "\n" + resCost.get(i) + "\n");
		}
		write.append("}\n");
		for(int i = 0; i < tradeCost.size(); ++i)
		{
			write.append(tradeCost.get(i) + "\n" + tradeCostAmount.get(i) + "\n");
		}
		write.append("}\n" + model + "\n");
		for(int i = 0; i < researches.size(); ++i)
		{
			write.append(researches.get(i) + "\n");
		}
		write.append("}\n" + Integer.toString(buildTime) + "\n");
		
		try {
			w.write(write.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
