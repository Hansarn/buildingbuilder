package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;

public class LevelController {
	@FXML
	private ListView<String> resPriceList;
	@FXML
	private ListView<String> resOutputList;
	@FXML
	private ListView<String> resCostList;

	@FXML
	private ListView<String> resPriceAmount;
	@FXML
	private ListView<String> resOutputAmount;
	@FXML
	private ListView<String> resCostAmount;

	@FXML
	private ListView<String> tradePriceList;
	@FXML
	private ListView<String> tradeOutputList;
	@FXML
	private ListView<String> tradeCostList;

	@FXML
	private ListView<String> tradePriceAmount;
	@FXML
	private ListView<String> tradeOutputAmount;
	@FXML
	private ListView<String> tradeCostAmount;

	@FXML
	private ListView<String> researchList;

	@FXML
	private ComboBox<String> resourcesPrice;
	@FXML
	private ComboBox<String> resourcesOutput;
	@FXML
	private ComboBox<String> resourcesCost;

	@FXML
	private ComboBox<String> tradeGoodsPrice;
	@FXML
	private ComboBox<String> tradeGoodsOutput;
	@FXML
	private ComboBox<String> tradeGoodsCost;

	@FXML
	private ComboBox<String> models;

	@FXML
	private ComboBox<String> researches;

	@FXML
	private Spinner<Integer> resPriceTicker;
	@FXML
	private Spinner<Integer> resOutputTicker;
	@FXML
	private Spinner<Integer> resCostTicker;

	@FXML
	private Spinner<Integer> tradePriceTicker;
	@FXML
	private Spinner<Integer> tradeOutputTicker;
	@FXML
	private Spinner<Integer> tradeCostTicker;

	@FXML
	private Spinner<Integer> maxPawns;

	@FXML
	private Spinner<Integer> time;

	@FXML
	public TextField name;

	@FXML
	private TextArea description;

	@FXML
	private Label level;

	@SuppressWarnings("unused")
	private int buildingLevel;
	
	public ArrayList<String> resources;

	public void initialize() {

		resPriceTicker.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));
		resOutputTicker.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));
		resCostTicker.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));

		tradePriceTicker.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));
		tradeOutputTicker.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));
		tradeCostTicker.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));

		maxPawns.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));
		time.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0, 1));

		readFromFile();

	}

	public void setUp(int level) {
		this.level.setText("Level " + Integer.toString(level));
		buildingLevel = level;
	}

	private void readFromFile() {
		BufferedReader in = null;
		resources = new ArrayList<String>();
		ArrayList<String> tradeGoods = new ArrayList<String>();
		ArrayList<String> modelsList = new ArrayList<String>();
		ArrayList<String> research = new ArrayList<String>();
		try {
			in = new BufferedReader(new FileReader("ResourceNames.data"));
			String temp = "wda";
			while ((temp = in.readLine()) != null) {

				resources.add(temp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			in = new BufferedReader(new FileReader("TradeGoodsNames.data"));
			String temp = "wda";
			while ((temp = in.readLine()) != null) {

				tradeGoods.add(temp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			in = new BufferedReader(new FileReader("../ModelNames.data"));
			String temp = "wda";
			while ((temp = in.readLine()) != null) {

				modelsList.add(temp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			in = new BufferedReader(new FileReader("../ResearchNames.data"));
			String temp = "wda";
			while ((temp = in.readLine()) != null) {

				research.add(temp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (String r : resources) {

			resourcesPrice.getItems().add(r);
			resourcesOutput.getItems().add(r);
			resourcesCost.getItems().add(r);
		}
		for (String r : tradeGoods) {
			tradeGoodsCost.getItems().add(r);
			tradeGoodsPrice.getItems().add(r);
			tradeGoodsOutput.getItems().add(r);
		}
		for (String r : modelsList) {
			models.getItems().add(r);
		}
		for (String r : research) {
			researches.getItems().add(r);
		}

	}

	@FXML
	private void addResourcesPrice() {
	    String text = resPriceTicker.getEditor().getText();
	    SpinnerValueFactory<Integer> valueFactory = resPriceTicker.getValueFactory();
	    if (valueFactory != null) {
	      StringConverter<Integer> converter = valueFactory.getConverter();
	      if (converter != null) {
	          int value = ((Integer)converter.fromString(text)).intValue();
	          valueFactory.setValue(Integer.valueOf(value));
	      }
	    }
  		if (resourcesPrice.getSelectionModel().getSelectedItem() != null && resPriceTicker.getValue() > 0 
				&& !resPriceList.getItems().contains(resourcesPrice.getSelectionModel().getSelectedItem())) {
			resPriceList.getItems().add(resourcesPrice.getSelectionModel().getSelectedItem());
			resPriceAmount.getItems().add(Integer.toString(resPriceTicker.getValue()));
		}
	}
	
	@FXML
	private void addResourcesOutput() {
	    String text = resOutputTicker.getEditor().getText();
	    SpinnerValueFactory<Integer> valueFactory = resOutputTicker.getValueFactory();
	    if (valueFactory != null) {
	      StringConverter<Integer> converter = valueFactory.getConverter();
	      if (converter != null) {
	          int value = ((Integer)converter.fromString(text)).intValue();
	          valueFactory.setValue(Integer.valueOf(value));
	      }
		}
		if (resourcesOutput.getSelectionModel().getSelectedItem() != null && resOutputTicker.getValue() > 0 
				&& !resOutputList.getItems().contains(resourcesOutput.getSelectionModel().getSelectedItem())) {
			resOutputList.getItems().add(resourcesOutput.getSelectionModel().getSelectedItem());
			resOutputAmount.getItems().add(Integer.toString(resOutputTicker.getValue()));
		}
	}
	@FXML
	private void addResourcesCost() {
	    String text = resCostTicker.getEditor().getText();
	    SpinnerValueFactory<Integer> valueFactory = resCostTicker.getValueFactory();
	    if (valueFactory != null) {
	      StringConverter<Integer> converter = valueFactory.getConverter();
	      if (converter != null) {
	          int value = ((Integer)converter.fromString(text)).intValue();
	          valueFactory.setValue(Integer.valueOf(value));

	      }
		}
		if (resourcesCost.getSelectionModel().getSelectedItem() != null && resCostTicker.getValue() > 0 
				&& !resCostList.getItems().contains(resourcesCost.getSelectionModel().getSelectedItem())) {
			resCostList.getItems().add(resourcesCost.getSelectionModel().getSelectedItem());
			resCostAmount.getItems().add(Integer.toString(resCostTicker.getValue()));
		}
	}

	@FXML
	private void addTradeGoodsPrice() {
	    String text = tradePriceTicker.getEditor().getText();
	    SpinnerValueFactory<Integer> valueFactory = tradePriceTicker.getValueFactory();
	    if (valueFactory != null) {
	      StringConverter<Integer> converter = valueFactory.getConverter();
	      if (converter != null) {
	          int value = ((Integer)converter.fromString(text)).intValue();
	          valueFactory.setValue(Integer.valueOf(value));

	      }
		}
		if (tradeGoodsPrice.getSelectionModel().getSelectedItem() != null && tradePriceTicker.getValue() > 0 
				&& !tradePriceList.getItems().contains(tradeGoodsPrice.getSelectionModel().getSelectedItem())) {
			tradePriceList.getItems().add(tradeGoodsPrice.getSelectionModel().getSelectedItem());
			tradePriceAmount.getItems().add(Integer.toString(tradePriceTicker.getValue()));
		}
	}

	@FXML
	private void addTradeGoodsOutput() {
	    String text = tradeOutputTicker.getEditor().getText();
	    SpinnerValueFactory<Integer> valueFactory = tradeOutputTicker.getValueFactory();
	    if (valueFactory != null) {
	      StringConverter<Integer> converter = valueFactory.getConverter();
	      if (converter != null) {
	          int value = ((Integer)converter.fromString(text)).intValue();
	          valueFactory.setValue(Integer.valueOf(value));

	      }
		}
		if (tradeGoodsOutput.getSelectionModel().getSelectedItem() != null && tradeOutputTicker.getValue() > 0 
				&& !tradeOutputList.getItems().contains(tradeGoodsOutput.getSelectionModel().getSelectedItem())) {
			tradeOutputList.getItems().add(tradeGoodsOutput.getSelectionModel().getSelectedItem());
			tradeOutputAmount.getItems().add(Integer.toString(tradeOutputTicker.getValue()));
		}
	}
	
	@FXML
	private void addTradeGoodsCost() {
	    String text = tradeCostTicker.getEditor().getText();
	    SpinnerValueFactory<Integer> valueFactory = tradeCostTicker.getValueFactory();
	    if (valueFactory != null) {
	      StringConverter<Integer> converter = valueFactory.getConverter();
	      if (converter != null) {
	          int value = ((Integer)converter.fromString(text)).intValue();
	          valueFactory.setValue(Integer.valueOf(value));

	      }
		}
		if (tradeGoodsCost.getSelectionModel().getSelectedItem() != null && tradeCostTicker.getValue() > 0 
				&& !tradeCostList.getItems().contains(tradeGoodsCost.getSelectionModel().getSelectedItem())) {
			tradeCostList.getItems().add(tradeGoodsCost.getSelectionModel().getSelectedItem());
			tradeCostAmount.getItems().add(Integer.toString(tradeCostTicker.getValue()));
		}
	}
	@FXML
	private void addResearches() {
		if (researches.getSelectionModel().getSelectedItem() != null) {
			researchList.getItems().add(researches.getSelectionModel().getSelectedItem());
		}
	}
	@FXML
	private void removeResPrice()
	{
		resPriceAmount.getItems().remove(resPriceList.getSelectionModel().getSelectedIndex());
		resPriceList.getItems().remove(resPriceList.getSelectionModel().getSelectedIndex());
	}
	@FXML
	private void removeResOutput()
	{
		resOutputAmount.getItems().remove(resOutputList.getSelectionModel().getSelectedIndex());
		resOutputList.getItems().remove(resOutputList.getSelectionModel().getSelectedIndex());
	}
	@FXML
	private void removeResCost()
	{
		resCostAmount.getItems().remove(resCostList.getSelectionModel().getSelectedIndex());
		resCostList.getItems().remove(resCostList.getSelectionModel().getSelectedIndex());
	}
	@FXML
	private void removeTradePrice()
	{
		tradePriceAmount.getItems().remove(tradePriceList.getSelectionModel().getSelectedIndex());
		tradePriceList.getItems().remove(tradePriceList.getSelectionModel().getSelectedIndex());
	}
	@FXML
	private void removeTradeOutput()
	{
		tradePriceAmount.getItems().remove(tradePriceList.getSelectionModel().getSelectedIndex());
		tradePriceList.getItems().remove(tradePriceList.getSelectionModel().getSelectedIndex());
	}
	@FXML
	private void removeTradeCost()
	{
		tradeCostAmount.getItems().remove(tradeCostList.getSelectionModel().getSelectedIndex());
		tradeCostList.getItems().remove(tradeCostList.getSelectionModel().getSelectedIndex());
	}
	@FXML
	private void removeResearch()
	{
		researchList.getItems().remove(researchList.getSelectionModel().getSelectedIndex());
	}
	public void printToFile(BufferedWriter w) {

	}
	public void editLevel(BuildingLevel level)
	{
		for (int i = 0; i < level.resPrice.size(); ++i)
		{
			resPriceList.getItems().add(level.resPrice.get(i));
			resPriceAmount.getItems().add(level.resPriceAmount.get(i));			
		}

		for (int i = 0; i < level.resOutput.size(); ++i)
		{
			resOutputList.getItems().add(level.resOutput.get(i));
			resOutputAmount.getItems().add(level.resOutputAmount.get(i));				
		}
		
		for (int i = 0; i < level.resCost.size(); ++i)
		{
			resCostList.getItems().add(level.resCost.get(i));
			resCostAmount.getItems().add(level.resCostAmount.get(i));				
		}
		
		for (int i = 0; i < level.tradePrice.size(); ++i)
		{
			tradePriceList.getItems().add(level.tradePrice.get(i));
			tradePriceAmount.getItems().add(level.tradePriceAmount.get(i));			
		}
		
		for (int i = 0; i < level.tradeOutput.size(); ++i)
		{
			tradeOutputList.getItems().add(level.tradeOutput.get(i));
			tradeOutputAmount.getItems().add(level.tradeOutputAmount.get(i));			
		}
		 
		for (int i = 0; i < level.tradeCost.size(); ++i)
		{
			tradeCostList.getItems().add(level.tradeCost.get(i));
			tradeCostAmount.getItems().add(level.tradeCostAmount.get(i));			
		}
		
		for (int i = 0; i < level.researches.size(); ++i)
		{

			
			researchList.getItems().add(level.researches.get(i));			
		}
		
		name.setText(level.name);
		description.setText(level.description);
		maxPawns.getValueFactory().setValue(level.maxPawns);
		time.getValueFactory().setValue(level.buildTime);
		models.getSelectionModel().select(level.model);
	}
	
	public void clearLevel()
	{
		resPriceList.getItems().clear();
		resPriceAmount.getItems().clear();
		resOutputList.getItems().clear();
		resOutputAmount.getItems().clear();
		resCostList.getItems().clear();
		resCostAmount.getItems().clear();
		tradePriceList.getItems().clear();
		tradePriceAmount.getItems().clear();
		tradeOutputList.getItems().clear();
		tradeOutputAmount.getItems().clear();
		tradeCostList.getItems().clear();
		tradeCostAmount.getItems().clear();
		
		researchList.getItems().clear();
		
		name.setText("");
		description.setText("");
		maxPawns.getValueFactory().setValue(0);
		time.getValueFactory().setValue(0);
		models.getSelectionModel().clearSelection();
	}
	
	public BuildingLevel saveLevel()
	{
		ArrayList<String> rPriceList = new ArrayList<String>(resPriceList.getItems());
		ArrayList<String> rPriceAmount = new ArrayList<String>(resPriceAmount.getItems());
		ArrayList<String> tPriceList = new ArrayList<String>(tradePriceList.getItems());
		ArrayList<String> tPriceAmount = new ArrayList<String>(tradePriceAmount.getItems());
		ArrayList<String> rOutputList = new ArrayList<String>(resOutputList.getItems());
		ArrayList<String> rOutputAmount = new ArrayList<String>(resOutputAmount.getItems());
		ArrayList<String> tOutputList = new ArrayList<String>(tradeOutputList.getItems());
		ArrayList<String> tOutputAmount = new ArrayList<String>(tradeOutputAmount.getItems());
		ArrayList<String> rCostList = new ArrayList<String>(resCostList.getItems());
		ArrayList<String> rCostAmount = new ArrayList<String>(resCostAmount.getItems());
		ArrayList<String> tCostList = new ArrayList<String>(tradeCostList.getItems());
		ArrayList<String> tCostAmount = new ArrayList<String>(tradeCostAmount.getItems());
		ArrayList<String> resList = new ArrayList<String>(researchList.getItems());
		
		BuildingLevel temp = new BuildingLevel(
							name.getText(),description.getText(),maxPawns.getValue(),
							rPriceList,rPriceAmount,tPriceList,tPriceAmount,
							rOutputList,rOutputAmount,tOutputList,tOutputAmount,
							rCostList,rCostAmount,tCostList,tCostAmount,
							models.getSelectionModel().getSelectedItem(),
							resList,time.getValue()		);
		
		return temp;
	}
}
