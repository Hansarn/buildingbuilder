package application;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Building {
	public ArrayList<BuildingLevel> levels;
	
	public int sizeX;
	public int sizeY;
	
	public String tileReq;
	
	public int type;
	
	public Building(int sizeX, int sizeY, String tileReq, int type, ArrayList<BuildingLevel> levels)
	{
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.tileReq = tileReq;
		this.type = type;
		this.levels = levels; 
		
	}
	
	public String getBuildingName(int i)
	{
		
		return levels.get(i).getName();
	}
	
	
	public void print(BufferedWriter w)
	{
		StringBuffer write = new StringBuffer();
		write.append(Integer.toString(sizeX) + "\n" + Integer.toString(sizeY) + "\n" + 
						tileReq + "\n" + Integer.toString(type) + "\n");
		
		try {
			w.write(write.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < 3; ++i)
		{
			levels.get(i).print(w);
		}
		try {
			w.write("\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
